<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('NoTransaksi', 8);
            $table->char('NIK', 13);
            $table->date('Tanggal_Pesan');
            $table->date('Tanggal_Pinjam');
            $table->date('Tanggal_Kembali_Rencana');
            $table->date('Tanggal_Kembali_Sebenarnya');
            $table->integer('LamaRental');
            $table->integer('LamaDenda');
            $table->text('Kerusakan');
            $table->char('IdSopir', 6);
            $table->double('BiayaBBM');
            $table->double('BiayaKerusakan');
            $table->double('Denda');
            $table->double('Total_Bayar');
            $table->double('Jumlah_Bayar');
            $table->double('Kembalian');
            $table->enum('StatusTransaksi', ['Proses', 'Mulai', 'Batal', 'Arsip', 'Selesai']);
            $table->integer('Arsip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
