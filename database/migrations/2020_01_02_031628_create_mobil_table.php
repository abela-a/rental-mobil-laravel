<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobil', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('NoPlat', 10);
            $table->string('KdMerk', 20);
            $table->string('IdType', 20);
            $table->enum('StatusRental', ['Jalan', 'Dipesan', 'Kosong']);
            $table->double('HargaSewa');
            $table->string('JenisMobil', 20);
            $table->enum('Transmisi', ['Manual', 'CVT', 'Matic']);
            $table->string('FotoMobil', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobil');
    }
}
