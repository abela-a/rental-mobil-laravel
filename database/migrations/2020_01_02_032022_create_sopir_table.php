<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSopirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sopir', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('IdSopir', 6);
            $table->char('NIK', 13);
            $table->string('NmSopir', 50);
            $table->text('Alamat');
            $table->char('NoTelp', 15);
            $table->enum('JenisKelamin', ['P', 'L']);
            $table->char('NoSim', 20);
            $table->double('TarifPerhari');
            $table->enum('StatusSopir', ['Sibuk', 'Dipesan', 'Luang']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sopir');
    }
}
