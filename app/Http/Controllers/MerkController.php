<?php

namespace App\Http\Controllers;

use App\Merk;
use Illuminate\Http\Request;

class MerkController extends Controller
{

    public function index()
    {
        $merk = Merk::all();
        return view('admin.merk', compact('merk'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'KdMerk' => 'required',
            'NmMerk' => 'required'
        ]);

        Merk::create([
            'KdMerk' => $request->KdMerk,
            'NmMerk' => $request->NmMerk
        ]);

        return redirect('admin/merk')->with('alert', 'Data merk berhasil ditambahkan!');
    }

    public function update(Request $request, Merk $merk)
    {
        $request->validate([
            'UbahNmMerk' => 'required'
        ]);

        Merk::where('id', $merk->id)
            ->update(
                [
                    'NmMerk' => $request->UbahNmMerk
                ]
            );

        return redirect('admin/merk')->with('alert', 'Data merk berhasil diubah!');
    }

    public function destroy(Merk $merk)
    {
        Merk::destroy($merk->id);
        return redirect('admin/merk')->with('alert', 'Data merk berhasil dihapus!');
    }
}
