<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ViewType;
use App\Type;
use App\Merk;

class TypeController extends Controller
{

    public function index()
    {
        $merk = Merk::all();
        $type = ViewType::all();
        return view('admin.type', compact('type', 'merk'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'KdMerk' => 'required',
            'IdType' => 'required',
            'NmType' => 'required'
        ]);

        Type::create([
            'IdType' => $request->IdType,
            'KdMerk' => $request->KdMerk,
            'NmType' => $request->NmType
        ]);

        return redirect('admin/tipe')->with('alert', 'Data tipe berhasil ditambahkan!');
    }

    public function update(Request $request, Type $type)
    {
        $request->validate([
            'UbahNmType' => 'required'
        ]);

        Type::where('id', $type->id)
            ->update(
                [
                    'NmType' => $request->UbahNmType
                ]
            );

        return redirect('admin/tipe')->with('alert', 'Data tipe berhasil diubah!');
    }

    public function destroy(Type $type)
    {
        Type::destroy($type->id);
        return redirect('admin/tipe')->with('alert', 'Data tipe berhasil dihapus!');
    }
}
