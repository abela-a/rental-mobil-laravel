<?php

namespace App\Http\Controllers;

use App\Sopir;
use Illuminate\Http\Request;

class SopirController extends Controller
{
    public function index()
    {
        $sopir = Sopir::all();
        return view('admin.sopir', compact('sopir'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'NIK' => 'required|min:12|max:13',
            'NmSopir' => 'required',
            'NoSim' => 'required',
            'JenisKelamin' => 'required',
            'Alamat' => 'required',
            'NoTelp' => 'required|min:10|max:15',
            'TarifPerhari' => 'required',
        ]);

        $query_sopir = Sopir::latest('IdSopir')->first();

        if ($query_sopir) {
            $latestSopir = $query_sopir->id_dokter;
            $latestSopir = ++$latestSopir;
        } else {
            Sopir::create([
                'IdSopir' => 'SPR000',
                'NIK' => '-',
                'NmSopir' => '-',
                'Alamat' => '-',
                'NoTelp' => '-',
                'JenisKelamin' => 'L',
                'NoSim' => '-',
                'TarifPerhari' => '0',
                'StatusSopir' => 'Luang',
            ]);
            $latestSopir = "SPR001";
        }

        $TarifPerhari = preg_replace('/\D/', '', $request->TarifPerhari);
        $NoTelp = preg_replace('/\D/', '', $request->NoTelp);

        Sopir::create([
            'IdSopir' => $latestSopir,
            'NIK' => $request->NIK,
            'NmSopir' => $request->NmSopir,
            'Alamat' => $request->Alamat,
            'NoTelp' => $NoTelp,
            'JenisKelamin' => $request->JenisKelamin,
            'NoSim' => $request->NoSim,
            'TarifPerhari' => $TarifPerhari,
            'StatusSopir' => 'Luang',
        ]);

        return redirect('admin/sopir')->with('alert', 'Data sopir berhasil ditambahkan!');
    }
    public function update(Request $request, Sopir $sopir)
    {
        $request->validate([
            'NmSopir' => 'required',
            'JenisKelamin' => 'required',
            'Alamat' => 'required',
            'NoTelp' => 'required|min:10|max:15',
            'TarifPerhari' => 'required',
        ]);

        $TarifPerhari = preg_replace('/\D/', '', $request->TarifPerhari);
        $NoTelp = preg_replace('/\D/', '', $request->NoTelp);

        Sopir::where('id', $sopir->id)
            ->update(
                [
                    'NmSopir' => $request->NmSopir,
                    'Alamat' => $request->Alamat,
                    'NoTelp' => $NoTelp,
                    'JenisKelamin' => $request->JenisKelamin,
                    'TarifPerhari' => $TarifPerhari,
                ]
            );

        return redirect('admin/sopir')->with('alert', 'Data sopir berhasil diubah!');
    }
    public function destroy(Sopir $sopir)
    {
        Sopir::destroy($sopir->id);
        return redirect('admin/sopir')->with('alert', 'Data sopir berhasil dihapus!');
    }
}
