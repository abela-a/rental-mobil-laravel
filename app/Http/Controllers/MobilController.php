<?php

namespace App\Http\Controllers;

use App\Merk;
use App\Mobil;
use App\Type;
use App\ViewMobil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MobilController extends Controller
{
    public function index()
    {
        $mobil = ViewMobil::all();
        $merk = Merk::all();
        return view('admin.mobil', compact('mobil', 'merk'));
    }

    public function getType($KdMerk = '')
    {
        $type = Type::where('KdMerk', $KdMerk)->get();
        return view('admin.addons.getType', compact('type'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'NoPlat' => 'required',
            'KdMerk' => 'required',
            'IdType' => 'required',
            'JenisMobil' => 'required',
            'Transmisi' => 'required',
            'HargaSewa' => 'required',
            'FotoMobil' => 'file|image|mimes:jpeg,png,jpg|max:500'
        ]);

        if ($request->FotoMobil != null) {
            $FotoMobil = $request->file('FotoMobil');
            $nama_file = 'FotoMobil-' . time() . '-' . $FotoMobil->getClientOriginalName();
            $tujuan_upload = 'img/fotomobil';
            $FotoMobil->move($tujuan_upload, $nama_file);
        } else {
            $nama_file = 'default.png';
        }

        $HargaSewa = preg_replace('/\D/', '', $request->HargaSewa);

        Mobil::create([
            'NoPlat' => $request->NoPlat,
            'KdMerk' => $request->KdMerk,
            'IdType' => $request->IdType,
            'JenisMobil' => $request->JenisMobil,
            'Transmisi' => $request->Transmisi,
            'HargaSewa' => $HargaSewa,
            'StatusRental' => 'Kosong',
            'FotoMobil' => $nama_file
        ]);

        return redirect('admin/mobil')->with('alert', 'Data mobil berhasil ditambahkan!');
    }

    public function update(Request $request, Mobil $mobil)
    {
        $request->validate([
            'JenisMobil' => 'required',
            'Transmisi' => 'required',
            'HargaSewa' => 'required',
            'FotoMobil' => 'file|image|mimes:jpeg,png,jpg|max:500'
        ]);

        if ($request->FotoMobil != null) {
            $FotoMobil = $request->file('FotoMobil');
            $nama_file = 'FotoMobil-' . time() . '-' . $FotoMobil->getClientOriginalName();
            $tujuan_upload = 'img/fotomobil';
            $FotoMobil->move($tujuan_upload, $nama_file);

            if ($mobil->FotoMobil != 'default.png') {
                File::delete($tujuan_upload . '/' . $mobil->FotoMobil);
            }
        } else {
            $nama_file = $mobil->FotoMobil;
        }

        $HargaSewa = preg_replace('/\D/', '', $request->HargaSewa);

        Mobil::where('id', $mobil->id)->update([
            'JenisMobil' => $request->JenisMobil,
            'Transmisi' => $request->Transmisi,
            'HargaSewa' => $HargaSewa,
            'FotoMobil' => $nama_file
        ]);

        return redirect('admin/mobil')->with('alert', 'Data mobil berhasil diubah!');
    }
    public function destroy(Mobil $mobil)
    {
        $tujuan_hapus = 'img/fotomobil';

        if ($mobil->FotoMobil != 'default.png') {
            File::delete($tujuan_hapus . '/' . $mobil->FotoMobil);
        }

        Mobil::destroy($mobil->id);
        return redirect('admin/mobil')->with('alert', 'Data mobil berhasil dihapus!');
    }
}
