<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class AccountController extends Controller
{
    public function pelanggan_index()
    {
        $pelanggan = User::where('RoleId', 3)->get();
        return view('admin.pelanggan', compact('pelanggan'));
    }
    public function pelanggan_store(Request $request)
    {
        $request->validate([
            'NIK' => 'required|min:12|max:13',
            'nama' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'min:8',
            'JenisKelamin' => 'required',
            'Alamat' => 'required',
            'NoTelp' => 'required'
        ]);

        $NoTelp = preg_replace('/\D/', '', $request->NoTelp);

        User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'NIK' => $request->NIK,
            'JenisKelamin' => $request->JenisKelamin,
            'NoTelp' => $NoTelp,
            'Alamat' => $request->Alamat,
            'Foto' => 'default.png',
            'RoleId' => 3,
        ]);

        return redirect('admin/pelanggan')->with('alert', 'Data pelanggan berhasil ditambahkan!');
    }
    public function pelanggan_update(Request $request, User $pelanggan)
    {
        $request->validate([
            'nama' => 'required',
            'JenisKelamin' => 'required',
            'Alamat' => 'required',
            'NoTelp' => 'required'
        ]);

        if ($request->password == null) {
            $password = $pelanggan->password;
        } else {
            $password = Hash::make($request->password);
        }

        $NoTelp = preg_replace('/\D/', '', $request->NoTelp);

        User::where('id', $pelanggan->id)
            ->update(
                [
                    'nama' => $request->nama,
                    'password' => $password,
                    'JenisKelamin' => $request->JenisKelamin,
                    'NoTelp' => $NoTelp,
                    'Alamat' => $request->Alamat,
                ]
            );

        return redirect('admin/pelanggan')->with('alert', 'Data pelanggan berhasil diubah!');
    }
    public function pelanggan_destroy(User $pelanggan)
    {
        User::destroy($pelanggan->id);
        return redirect('admin/pelanggan')->with('alert', 'Data pelanggan berhasil dihapus!');
    }
    public function karyawan_index()
    {
        $karyawan = User::where('RoleId', 2)->get();
        return view('admin.karyawan', compact('karyawan'));
    }
    public function karyawan_store(Request $request)
    {
        $request->validate([
            'NIK' => 'required|min:12|max:13',
            'nama' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'min:8',
            'JenisKelamin' => 'required',
            'Alamat' => 'required',
            'NoTelp' => 'required'
        ]);

        $NoTelp = preg_replace('/\D/', '', $request->NoTelp);

        User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'NIK' => $request->NIK,
            'JenisKelamin' => $request->JenisKelamin,
            'NoTelp' => $NoTelp,
            'Alamat' => $request->Alamat,
            'Foto' => 'default.png',
            'RoleId' => 2,
        ]);

        return redirect('admin/karyawan')->with('alert', 'Data karyawan berhasil ditambahkan!');
    }
    public function karyawan_update(Request $request, User $karyawan)
    {
        $request->validate([
            'nama' => 'required',
            'JenisKelamin' => 'required',
            'Alamat' => 'required',
            'NoTelp' => 'required'
        ]);

        if ($request->password == null) {
            $password = $karyawan->password;
        } else {
            $password = Hash::make($request->password);
        }

        $NoTelp = preg_replace('/\D/', '', $request->NoTelp);

        User::where('id', $karyawan->id)
            ->update(
                [
                    'nama' => $request->nama,
                    'password' => $password,
                    'JenisKelamin' => $request->JenisKelamin,
                    'NoTelp' => $NoTelp,
                    'Alamat' => $request->Alamat,
                ]
            );

        return redirect('admin/karyawan')->with('alert', 'Data karyawan berhasil diubah!');
    }
    public function karyawan_destroy(User $karyawan)
    {
        User::destroy($karyawan->id);
        return redirect('admin/karyawan')->with('alert', 'Data karyawan berhasil dihapus!');
    }
    public function role_index()
    {
        $role = User::all();
        return view('admin.role', compact('role'));
    }
    public function role_update(Request $request, User $role)
    {
        User::where('id', $role->id)
            ->update(
                [
                    'RoleId' => $request->role
                ]
            );

        return redirect('admin/role')->with('alert', 'Data role berhasil diubah!');
    }
}
