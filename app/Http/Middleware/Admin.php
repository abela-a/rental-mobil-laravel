<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (Auth::user()->RoleId == 1) {
            return $next($request);
        }

        if (Auth::user()->RoleId == 2) {
            return redirect()->route('karyawan.dashboard');
        }

        if (Auth::user()->RoleId == 3) {
            return redirect()->route('pelanggan.dashboard');
        }
    }
}
