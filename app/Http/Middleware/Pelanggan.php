<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Pelanggan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (Auth::user()->RoleId == 1) {
            return redirect()->route('admin.dashboard');
        }

        if (Auth::user()->RoleId == 2) {
            return redirect()->route('karyawan.dashboard');
        }

        if (Auth::user()->RoleId == 3) {
            return $next($request);
        }
    }
}
