<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sopir extends Model
{
    protected $table = 'sopir';
    protected $fillable = ['IdSopir', 'NmSopir', 'NIK', 'Alamat', 'NoTelp', 'JenisKelamin', 'NoSim', 'TarifPerhari', 'StatusSopir'];
}
