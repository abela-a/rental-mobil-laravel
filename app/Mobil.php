<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    protected $table = 'mobil';
    protected $fillable = ['NoPlat', 'KdMerk', 'IdType', 'StatusRental', 'HargaSewa', 'JenisMobil', 'Transmisi', 'FotoMobil'];
}
