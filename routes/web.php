<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::view('', 'pages.index')->name('homepage');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    // DASHBOARD
    Route::get('', 'AdminMasterController@index')->name('admin.dashboard');
    // PROFILE
    Route::get('profile', 'AdminMasterController@index')->name('admin.profile');
    // MERK
    Route::group(['prefix' => 'merk'], function () {
        Route::get('', 'MerkController@index')->name('admin.merk');
        Route::post('', 'MerkController@store')->name('admin.merk.simpan');
        Route::delete('{merk}', 'MerkController@destroy');
        Route::patch('{merk}', 'MerkController@update');
    });
    // TYPE
    Route::group(['prefix' => 'tipe'], function () {
        Route::get('', 'TypeController@index')->name('admin.type');
        Route::post('', 'TypeController@store')->name('admin.type.simpan');
        Route::delete('{type}', 'TypeController@destroy');
        Route::patch('{type}', 'TypeController@update');
    });
    // MOBIL
    Route::group(['prefix' => 'mobil'], function () {
        Route::get('', 'MobilController@index')->name('admin.mobil');
        Route::post('', 'MobilController@store')->name('admin.mobil.simpan');
        Route::get('type/{KdMerk}', 'MobilController@getType');
        Route::delete('{mobil}', 'MobilController@destroy');
        Route::patch('{mobil}', 'MobilController@update');
    });
    // SOPIR
    Route::group(['prefix' => 'sopir'], function () {
        Route::get('', 'SopirController@index')->name('admin.sopir');
        Route::post('', 'SopirController@store')->name('admin.sopir.simpan');
        Route::delete('{sopir}', 'SopirController@destroy');
        Route::patch('{sopir}', 'SopirController@update');
    });
    // PELANGGAN
    Route::group(['prefix' => 'pelanggan'], function () {
        Route::get('', 'AccountController@pelanggan_index')->name('admin.pelanggan');
        Route::post('', 'AccountController@pelanggan_store')->name('admin.pelanggan.simpan');
        Route::delete('{pelanggan}', 'AccountController@pelanggan_destroy');
        Route::patch('{pelanggan}', 'AccountController@pelanggan_update');
    });
    // KARYAWAN
    Route::group(['prefix' => 'karyawan'], function () {
        Route::get('', 'AccountController@karyawan_index')->name('admin.karyawan');
        Route::post('', 'AccountController@karyawan_store')->name('admin.karyawan.simpan');
        Route::delete('{karyawan}', 'AccountController@karyawan_destroy');
        Route::patch('{karyawan}', 'AccountController@karyawan_update');
    });
    // ROLE
    Route::group(['prefix' => 'role'], function () {
        Route::get('', 'AccountController@role_index')->name('admin.role');
        Route::patch('{role}', 'AccountController@role_update');
    });
});

// KARYAWAN
Route::group(['prefix' => 'karyawan', 'middleware' => ['auth', 'karyawan']], function () {
    // DASHBOARD
    Route::get('', 'KaryawanMasterController@index')->name('karyawan.dashboard');
    // PROFILE
    Route::get('profile', 'KaryawanMasterController@index')->name('karyawan.profile');
});

// PELANGGAN
Route::group(['prefix' => 'pelanggan', 'middleware' => ['auth', 'pelanggan']], function () {
    // DASHBOARD
    Route::get('', 'PelangganMasterController@index')->name('pelanggan.dashboard');
    // PROFILE
    Route::get('profile', 'PelangganMasterController@index')->name('pelanggan.profile');
});
