<!--Main Navigation-->
@php
if(Auth::user()->RoleId === 1){
$role = 'Admin';
$profile = route('admin.profile');
} elseif(Auth::user()->RoleId === 2){
$role = 'Karyawan';
$profile = route('karyawan.profile');
} else {
$role = 'Pelanggan';
$profile = route('pelanggan.profile');
}
@endphp

<header>
  <nav class="navbar navbar-expand-lg navbar-dark indigo accent-2 py-2 shadow-none">
    <div class="container">

      <a class="navbar-brand py-0" href="{{ route('homepage') }}">
        <img src="{{ asset('img/assets/logo.png') }}" width="40" height="40" class="d-inline-block align-top" alt="">
        <span style="font-size:25px" class="font-weight-bold">Abidzar</span>
        <span style="font-size:18px" class="font-weight-thin">Car Rental</span>
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navResponsive"
        aria-controls="navResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse ml-4" id="navResponsive">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item dropdown">
            <a class="nav-link" id="mobil" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="badge badge-light shadow-none mr-2">{{ $role }}</span>
              <span style="font-size:15px" class="text-white mr-2 font-weight-regular">
                {{ Auth::user()->nama }}
              </span>
              <img src="{{ asset('img/fotouser') }}/{{ Auth::user()->Foto }}" class="rounded border" width="35"
                height="35">
            </a>
            <div class="dropdown-menu dropdown-warning dropdown-menu-right" aria-labelledby="mobil">
              <a class="dropdown-item" href="{{ $profile }}">
                Edit Profile
              </a>
              <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<!--Main Navigation-->

<!-- Nav -->
<div class="indigo sticky-top" id="navdashboard" style="z-index:1">
  <div class="container py-1">
    <ul id="no-waves" class="nav md-tabs justify-content-center indigo shadow-none mx-0 mb-0">

      <li class="nav-item mr-2">
        <a class="nav-link {{ Request::is('admin') ? 'nav-dashboard-active rounded' : '' }}"
          href="{{ route('admin.dashboard') }}">
          <i class="fas fa-tachometer-alt fa-fw mr-1"></i>
          Dashboard
        </a>
      </li>

      <li class="nav-item dropdown mr-2">
        <a class="nav-link dropdown-toggle {{ Request::is('admin/merk') || Request::is('admin/tipe') || Request::is('admin/mobil') ? 'nav-dashboard-active rounded' : '' }}"
          data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-car fa-fw mr-1"></i>
          Kendaraan
        </a>
        <div class="dropdown-menu dropdown-primary">
          <a class="dropdown-item {{ Request::is('admin/merk') ? 'active' : '' }}"
            href="{{ route('admin.merk') }}">Merk</a>
          <a class="dropdown-item {{ Request::is('admin/tipe') ? 'active' : '' }}"
            href="{{ route('admin.type') }}">Type</a>
          <a class="dropdown-item {{ Request::is('admin/mobil') ? 'active' : '' }}"
            href="{{ route('admin.mobil') }}">Mobil</a> </div>
      </li>

      <li class="nav-item dropdown mr-2">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
          aria-expanded="false">
          <i class="fas fa-dollar-sign fa-fw mr-1"></i>
          Transaksi
        </a>
        <div class="dropdown-menu dropdown-primary">
          <a class="dropdown-item" href="/admin/pemesanan">
            Pemesanan
          </a>
          <a class="dropdown-item " href="/admin/transaksi">Transaksi</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item " href="/admin/arsip_transaksi">Arsip Transaksi</a>
        </div>
      </li>

      <li class="nav-item dropdown mr-2">
        <a class="nav-link dropdown-toggle {{ Request::is('admin/pelanggan') || Request::is('admin/karyawan') || Request::is('admin/role') ? 'nav-dashboard-active rounded' : '' }}"
          data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-users fa-fw mr-3"></i>
          Data Akun
        </a>
        <div class="dropdown-menu dropdown-primary">
          <a class="dropdown-item {{ Request::is('admin/pelanggan') ? 'active' : '' }}"
            href="{{ route('admin.pelanggan') }}">Pelanggan</a>
          <a class="dropdown-item {{ Request::is('admin/karyawan') ? 'active' : '' }}"
            href="{{ route('admin.karyawan') }}">Karyawan</a>
          <div class="dropdown-divider {{ Request::is('admin/role') ? 'active' : '' }}"></div>
          <a class="dropdown-item" href="{{ route('admin.role') }}">Manajemen Akses</a>
        </div>
      </li>

      <li class="nav-item mr-2">
        <a class="nav-link {{ Request::is('admin/sopir*') ? 'nav-dashboard-active rounded' : '' }}"
          href="{{ route('admin.sopir') }}">
          <i class="fas fa-user-tie fa-fw mr-1"></i>
          Data Sopir
        </a>
      </li>

      <li class="nav-item mr-2">
        <a class="nav-link" href="/laporan">
          <i class="fas fa-print fa-fw mr-1"></i>
          Laporan
        </a>
      </li>

    </ul>
  </div>
</div>
<!-- Akhir Nav -->

<div class="indigo shadow-sm" id="top-main">
  <div class="container">
    <div class="pt-5 pb-1">
      <h2 class="h1 text-white">@yield('heading')</h2>
    </div>
    <div class="pb-5" id="breadcrump">
      @yield('breadcrumb')
    </div>
  </div>
</div>