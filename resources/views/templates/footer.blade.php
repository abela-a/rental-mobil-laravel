<!-- FOOTER -->
<footer class="mt-5 rounded-top unique-color-dark py-2">
  <div class="text-center">
    Copyright &copy; <b>Abidzar</b> Car Rental <?= date('Y') ?>
  </div>
</footer>