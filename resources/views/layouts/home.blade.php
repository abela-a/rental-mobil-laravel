<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="{{ asset('vendor/MaterialBootstrap/font/fontawesome/css/all.css') }}">
  <!-- BS4 CORE -->
  <link rel="stylesheet" href="{{ asset('vendor/MaterialBootstrap/css/bootstrap.min.css') }}">
  <!-- MATERIAL DESIGN -->
  <link rel="stylesheet" href="{{ asset('vendor/MaterialBootstrap/css/mdb.min.css') }}">
  <!-- DATATABLES -->
  <link rel="stylesheet" href="{{ asset('vendor/MaterialBootstrap/css/addons/datatables.css') }}">
  <!-- ANIMATE.CSS -->
  <link rel="stylesheet" href="{{ asset('vendor/Animate.css/animate.css') }}">
  <!-- MY STYLE -->
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  <!-- CUSTOM COLOR -->
  <link rel="stylesheet" href="{{ asset('css/colors.css') }}">
  <!-- FAVICON -->
  <link rel="shortcut icon" href="{{ asset('img/assets/icon.png') }}" type="image/x-icon">

  <title>@yield('title')</title>
</head>

<body class="bg-gray">

  @include('pages.templates.navbar')

  @yield('content')

  @include('pages.templates.footer')

  <!-- LOADER -->
  <div id="mdb-preloader" class="flex-center">
    <div class="spinner-grow text-info" role="status">
      <span class="sr-only"></span>
    </div>
    <div class="spinner-grow text-primary" role="status">
      <span class="sr-only">Loading...</span>
    </div>
    <div class="spinner-grow text-warning" role="status">
      <span class="sr-only"></span>
    </div>
  </div>

  <!-- JQUERY -->
  <script src="{{ asset('vendor/MaterialBootstrap/js/jquery-3.4.1.min.js') }}">
  </script>
  <!-- POPPER -->
  <script src="{{ asset('vendor/MaterialBootstrap/js/popper.min.js') }}"></script>
  <!-- BS4 CORE -->
  <script src="{{ asset('vendor/MaterialBootstrap/js/bootstrap.min.js') }}"></script>
  <!-- MATERIAL DESIGN -->
  <script src="{{ asset('vendor/MaterialBootstrap/js/mdb.min.js') }}"></script>
  <!-- DATATABLES -->
  <script src="{{ asset('vendor/MaterialBootstrap/js/addons/datatables.js') }}"></script>
  <!-- SWEET ALERT -->
  <script src="{{ asset('vendor/SweetAlert2/dist/sweetalert2.all.min.js') }}"></script>
  <!-- MASK -->
  <script src="{{ asset('vendor/MaterialBootstrap/js/jquery.mask.min.js') }}"></script>
  <!--MOMENT.JS -->
  <script src="{{ asset('vendor/Moment.js/moment.js') }}"></script>
  <!--DATEPICKER -->
  <script src="{{ asset('js/datepicker_sett.js') }}"></script>
  <!--TRANSAKSI -->
  <script src="{{ asset('js/transaksi.js') }}"></script>
  <!-- MY SCRIPT -->
  <script src="{{ asset('js/script.js') }}"></script>

  @yield('get-stuff')

</body>

</html>