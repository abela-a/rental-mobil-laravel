Pelanggan
<a href="{{ route('logout') }}" class="btn btn-danger btn-lg btn-block btn-icon-split"
  onclick="event.preventDefault();document.getElementById('logout-form').submit();">
  <i class="fas fa-sign-out-alt"></i> Logout
</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>