@extends('layouts/dashboard')

@section('title', 'Tipe - Abidzar Car Rental')
@section('heading', 'Daftar Tipe Mobil')
@section('breadcrumb')
<div style="font-size:17px" class="text-white">
  <i class="fa fa-home fa-fw"></i>
  <span class="mx-3">|</span>
  <a href="{{ route('homepage') }}" class="text-white">Home</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <a href="{{ route('admin.dashboard') }}" class="text-white">Admin</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <span>Tipe</span>
</div>
@endsection

@section('content')
<!-- TOMBOL -->
<div class="container" id="main-menu">
  <div class="row mb-4">
    <div class="col-md">
      <button class="btn btn-primary shadow-none mb-3 float-right" data-toggle="modal" data-target="#inputTipe">
        <i class="fa fa-plus fa-fw"></i> Tambah Tipe
      </button>
    </div>
  </div>
  <div class="bg-white shadow-sm rounded pt-5 pb-4 px-5">

    @if(session('alert'))
    <div class="alert alert-success shadow-sm">
      {{ session('alert') }}
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
    </div>
    @endif

    <table class="table table-striped" id="tolong">
      <thead>
        <tr>
          <th>#</th>
          <th>Kode Tipe</th>
          <th>Nama Tipe</th>
          <th>Nama Merk</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($type as $t)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $t->IdType }}</td>
          <td>{{ $t->NmType }}</td>
          <td>{{ $t->NmMerk }}</td>
          <td style="width:150px">
            <button class="btn btn-sm btn-warning shadow-none" data-toggle="modal" data-target="#editTipe"
              data-id="{{ $t->id }}" data-idtipe="{{ $t->IdType }}" data-namatipe="{{ $t->NmType }}"
              data-namamerk="{{ $t->NmMerk }}">
              <i class="fas fa-fw fa-edit"></i>
            </button>
            <button class="btn btn-sm btn-danger shadow-none" data-toggle="modal" data-target="#hapusTipe"
              data-id="{{ $t->id }}">
              <i class="fas fa-fw fa-trash"></i>
            </button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<!-- AWAL MODAL TAMBAH-->
<div class="modal fade" id="inputTipe">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-primary text-center">
        <h5 class="modal-title h5 w-100">TAMBAH DATA TIPE</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <!-- AWAL FORM -->
        <form action="{{ route('admin.type.simpan') }}" method="post">
          @csrf
          <div class="form-group">
            <label for="KdMerk">Merk</label>
            <select class="browser-default custom-select @error('KdMerk') is-invalid @enderror" name="KdMerk"
              id="KdMerk">
              <option value="" selected disabled>Pilih Merk</option>
              @foreach ($merk as $m)
              <option @if(old('KdMerk')==$m->KdMerk) selected @endif value="{{ $m->KdMerk }}">{{ $m->NmMerk }}</option>
              @endforeach
            </select>
            <div class="invalid-feedback">
              Merk wajib dipilih
            </div>
          </div>
          <div class="form-group">
            <label for="IdType">ID Tipe</label>
            <input type="text" class="form-control @error('IdType') is-invalid @enderror" id="IdType" name="IdType"
              autocomplete="off" value="{{ old('IdType') }}">
            <div class="invalid-feedback">
              ID tipe wajib diisi
            </div>
          </div>
          <div class="form-group">
            <label for="NmType">Nama Tipe</label>
            <input type="text" class="form-control @error('NmType') is-invalid @enderror" id="NmType" name="NmType"
              autocomplete="off" value="{{ old('NmType') }}">
            <div class="invalid-feedback">
              Nama tipe wajib diisi
            </div>
          </div>
          <!-- AKHIR FORM -->
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary shadow-none" id="simpan">Simpan Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
<!-- AKHIR MODAL TAMBAH-->

<!-- AWAL MODAL EDIT-->
<div class="modal fade" id="editTipe">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-primary text-center">
        <h5 class="modal-title h5 w-100">UBAH DATA TIPE</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <!-- AWAL FORM -->
        <form action="#" method="post" id="editTipeForm">
          @csrf
          @method('patch')
          <div class="form-group">
            <label for="NmMerk">Nama Merk</label>
            <input type="text" class="form-control" id="UbahNmMerk" value="" disabled>
          </div>
          <div class="form-group">
            <label for="IdType">ID Tipe</label>
            <input type="text" class="form-control " id="UbahIdType" value="" disabled>
          </div>
          <div class="form-group">
            <label for="NmType">Nama Tipe</label>
            <input type="text" class="form-control @error('UbahNmType') is-invalid @enderror" id="UbahNmType"
              name="UbahNmType" autocomplete="off" value="">
            <div class="invalid-feedback">
              Nama tipe wajib diisi
            </div>
          </div>
          <!-- AKHIR FORM -->
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary shadow-none" id="submit">Ubah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR MODAL EDIT-->

<!-- AWAL HAPUS -->
<div class="modal fade center" id="hapusTipe">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-danger text-center">
        <h5 class="modal-title h5 w-100">HAPUS DATA TIPE</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="#" method="post" id="hapusTipeForm">
          @csrf
          @method('delete')
          <center>
            <h5>Data ini akan dihapus. Apakah anda yakin?</h5>
          </center>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="submit" class="btn btn-danger shadow-none">Ya</button>
        <button class="btn btn-outline-danger shadow-none" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR HAPUS -->
@endsection

@section('script')
<script>
  $(document).ready(function(){
    // EDIT
    $('#editTipe').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var id = button.data('id') 
      var IdType = button.data('idtipe') 
      var NmType = button.data('namatipe') 
      var NmMerk = button.data('namamerk') 

      var modal = $(this)
      $('#editTipeForm').attr('action', '{{ route("admin.type") }}/'+id)
      modal.find('.modal-body #UbahIdType').val(IdType)
      modal.find('.modal-body #UbahNmType').val(NmType)
      modal.find('.modal-body #UbahNmMerk').val(NmMerk)
    })
    // HAPUS
    $('#hapusTipe').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var id = button.data('id') 

      var modal = $(this)
      $('#hapusTipeForm').attr('action', '{{ route("admin.type") }}/'+id)
    })
  })
</script>
@endsection