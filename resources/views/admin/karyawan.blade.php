@extends('layouts/dashboard')

@section('title', 'Karyawan - Abidzar Car Rental')
@section('heading', 'Daftar Karyawan')
@section('breadcrumb')
<div style="font-size:17px" class="text-white">
  <i class="fa fa-home fa-fw"></i>
  <span class="mx-3">|</span>
  <a href="{{ route('homepage') }}" class="text-white">Home</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <a href="{{ route('admin.dashboard') }}" class="text-white">Admin</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <span>Karyawan</span>
</div>
@endsection

@section('content')
<!-- TOMBOL -->
<div class="container" id="main-menu">
  <div class="row mb-4">
    <div class="col-md">
      <button class="btn btn-primary shadow-none mb-3 float-right" type="button" data-toggle="modal"
        data-target="#inputKaryawan">
        <i class="fa fa-plus fa-fw"></i> Tambah Karyawan
      </button>
    </div>
  </div>
  <div class="bg-white shadow-sm rounded pt-5 pb-4 px-5">
    @if(session('alert'))
    <div class="alert alert-success shadow-sm">
      {{ session('alert') }}
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
    </div>
    @endif
    <!-- TABLE -->
    <table class="table table-striped" id="tolong">
      <thead>
        <tr>
          <th>#</th>
          <th>NIK</th>
          <th>Nama</th>
          <th>Nomor Telepon</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($karyawan as $k)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $k->NIK }}</td>
          <td> {{ $k->nama }} </td>
          <td><span class="telp">{{ $k->NoTelp }}</span></td>
          <td class="text-center" style="width:220px">
            <button class="btn btn-sm btn-warning shadow-none" data-toggle="modal"
              data-target="#editKaryawan-{{ $k->id }}">
              <i class="fas fa-fw fa-edit"></i>
            </button>
            <button class="btn btn-sm btn-danger shadow-none" data-toggle="modal" data-target="#hapusKaryawan"
              data-id="{{ $k->id }}">
              <i class="fas fa-fw fa-trash"></i>
            </button>
            <button class="btn btn-sm btn-info shadow-none" data-toggle="modal"
              data-target="#detailKaryawan-{{ $k->id }}">
              <i class="fas fa-fw fa-user"></i>
            </button>
          </td>
        </tr>

        <!-- AWAL MODAL EDIT-->
        <div class="modal fade" id="editKaryawan-{{ $k->id }}">
          <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header text-primary text-center">
                <h5 class="modal-title h5 w-100">UBAH DATA KARYAWAN</h5>
              </div>
              <div class="modal-body px-5 grey lighten-5">
                <form action="{{ route('admin.karyawan') }}/{{ $k->id }}" method="post">
                  @csrf @method('patch')
                  <div class="form-group">
                    <label for="NIK">Nomor Induk Kependudukan</label>
                    <input type="text" class="form-control" id="NIK" value="{{ $k->NIK }}" disabled>
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" value="{{ $k->email }}" disabled>
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                      id="password" autocomplete="off" placeholder="Kosongkan jika tidak ingin mengubah.">
                    <div class="invalid-feedback">
                      Password minimal 8 karakter.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama"
                      autocomplete="off" value="{{ $k->nama }}">
                    <div class="invalid-feedback">
                      Nama lengkap wajib diisi.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="JenisKelamin">Jenis Kelamin</label>
                    <select name="JenisKelamin" id="JenisKelamin"
                      class="form-control browser-default @error('JenisKelamin') is-invalid @enderror">
                      <option disabled selected>Pilih Jenis Kelamin</option>
                      <option @if($k->JenisKelamin=="L" ) selected @endif value="L">Laki-laki</option>
                      <option @if($k->JenisKelamin=="P" ) selected @endif value="P">Perempuan</option>
                    </select>
                    <div class="invalid-feedback">
                      Jenis kelamin wajib dipilih.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="Alamat">Alamat</label>
                    <textarea name="Alamat" id="Alamat" class="form-control @error('Alamat') is-invalid @enderror"
                      autocomplete="off">{{ $k->Alamat }}</textarea>
                    <div class="invalid-feedback">
                      Alamat wajib diisi.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="NoTelp">Nomor Telepon</label>
                    <input type="text" class="form-control telp @error('NoTelp') is-invalid @enderror" name="NoTelp"
                      id="NoTelp" autocomplete="off" value="{{ $k->NoTelp }}">
                    <div class="invalid-feedback">
                      Nomor HP/telepon wajib diisi.
                    </div>
                  </div>
              </div>
              <div class="modal-footer text-center justify-content-center">
                <button type="button" class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
                <button type="submit" class="btn btn-primary shadow-none" id="ubah">Ubah Data</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- AKHIR MODAL EDIT-->

        <!-- AWAL MODAL DETAIL -->
        <div class="modal fade right" id="detailKaryawan-{{ $k->id }}">
          <div class="modal-dialog modal-full-height modal-right">
            <div class="modal-content">
              <div class="modal-header text-center text-primary">
                <h4 class="modal-title w-100 h5">DETAIL KARYAWAN</h4>
              </div>
              <div class="modal-body px-5 grey lighten-5">
                <ul class="list-group list-group-flush">
                  <div class="row list-group-item grey lighten-5">
                    <img width="50" src="{{ asset('img/fotouser') }}/{{ $k->Foto }}" class="card-img">
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Nomor Induk Kependudukan</div>
                    <div class="col" style="font-weight:500">{{ $k->NIK }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Nama</div>
                    <div class="col" style="font-weight:500">{{ $k->nama }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Email</div>
                    <div class="col" style="font-weight:500">{{ $k->email }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Jenis Kelamin</div>
                    <div class="col" style="font-weight:500">
                      @if ($k->JenisKelamin == 'L')
                      Laki-laki
                      @else
                      Perempuan
                      @endif
                    </div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">No Telepon</div>
                    <div class="col telp" style="font-weight:500">{{ $k->NoTelp }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Alamat</div>
                    <div class="col" style="font-weight:500">{{ $k->Alamat }}</div>
                  </div>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- AKHIR MODAL DETAIL -->

        @endforeach
      </tbody>
    </table>
    <!-- AKHIR TABLE -->
  </div>
</div>

<!-- AWAL MODAL-->
<div class="modal fade" id="inputKaryawan">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-primary text-center">
        <h5 class="modal-title h5 w-100" id="inputKaryawanLabel">TAMBAH DATA KARYAWAN</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="{{ route('admin.karyawan.simpan') }}" method="post">
          @csrf
          <div class="form-group">
            <label for="NIK">Nomor Induk Kependudukan</label>
            <input type="text" class="form-control @error('NIK') is-invalid @enderror" name="NIK" id="NIK"
              autocomplete="off" value="{{ old('NIK') }}">
            <div class="invalid-feedback">
              NIK wajib diisi dengan panjang 13 karakter.
            </div>
          </div>
          <div class="form-group">
            <label for="nama">Nama Lengkap</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama"
              autocomplete="off" value="{{ old('nama') }}">
            <div class="invalid-feedback">
              Nama lengkap wajib diisi.
            </div>
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email"
              autocomplete="off" value="{{ old('email') }}">
            <div class="invalid-feedback">
              Email harus valid.
            </div>
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
              id="password" autocomplete="off" value="{{ old('password') }}">
            <div class="invalid-feedback">
              Password minimal 8 karakter.
            </div>
          </div>
          <div class="form-group">
            <label for="JenisKelamin">Jenis Kelamin</label>
            <select name="JenisKelamin" id="JenisKelamin"
              class="form-control browser-default @error('JenisKelamin') is-invalid @enderror">
              <option disabled selected>Pilih Jenis Kelamin</option>
              <option @if(old('JenisKelamin')=="L" ) selected @endif value="L">Laki-laki</option>
              <option @if(old('JenisKelamin')=="P" ) selected @endif value="P">Perempuan</option>
            </select>
            <div class="invalid-feedback">
              Jenis kelamin wajib dipilih.
            </div>
          </div>
          <div class="form-group">
            <label for="Alamat">Alamat</label>
            <textarea name="Alamat" id="Alamat" class="form-control @error('Alamat') is-invalid @enderror"
              autocomplete="off">{{ old('Alamat') }}</textarea>
            <div class="invalid-feedback">
              Alamat wajib diisi.
            </div>
          </div>
          <div class="form-group">
            <label for="NoTelp">Nomor Telepon</label>
            <input type="text" class="form-control telp @error('NoTelp') is-invalid @enderror" name="NoTelp" id="NoTelp"
              autocomplete="off" value="{{ old('NoTelp') }}">
            <div class="invalid-feedback">
              Nomor HP/telepon wajib diisi.
            </div>
          </div>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="button" class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary shadow-none" id="submit">Simpan Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR MODAL-->

<!-- AWAL HAPUS -->
<div class="modal fade center" id="hapusKaryawan">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-danger text-center">
        <h5 class="modal-title h5 w-100">HAPUS DATA KARYAWAN</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="#" method="post" id="hapusKaryawanForm">
          @csrf
          @method('delete')
          <center>
            <h5>Data ini akan dihapus. Apakah anda yakin?</h5>
          </center>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="submit" class="btn btn-danger shadow-none">Ya</button>
        <button type="button" class="btn btn-outline-danger shadow-none" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR HAPUS -->

@endsection

@section('script')
<script>
  $(document).ready(function(){
    // HAPUS
    $('#hapusKaryawan').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var id = button.data('id') 

      var modal = $(this)
      $('#hapusKaryawanForm').attr('action', '{{ route("admin.karyawan") }}/'+id)
    })
  })
</script>
@endsection