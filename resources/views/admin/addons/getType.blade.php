<label for="IdType">Tipe</label>
<select class="browser-default custom-select @error('IdType') is-invalid @enderror" name="IdType" id="IdType">
  <option value="" selected disabled>Pilih Tipe Mobil</option>
  @foreach ($type as $t)
  <option @if(old('IdType')==$t->IdType) selected @endif value="{{ $t->IdType }}">{{ $t->NmType }}</option>
  @endforeach
</select>
<div class="invalid-feedback">
  Tipe mobil wajib dipilih.
</div>