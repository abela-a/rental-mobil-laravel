@extends('layouts/dashboard')

@section('title', 'Merk - Abidzar Car Rental')
@section('heading', 'Daftar Merk Mobil')
@section('breadcrumb')
<div style="font-size:17px" class="text-white">
  <i class="fa fa-home fa-fw"></i>
  <span class="mx-3">|</span>
  <a href="{{ route('homepage') }}" class="text-white">Home</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <a href="{{ route('admin.dashboard') }}" class="text-white">Admin</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <span>Merk</span>
</div>
@endsection

@section('content')
<!-- TOMBOL -->
<div class="container" id="main-menu">
  <div class="row mb-4">
    <div class="col-md">
      <button class="btn btn-primary shadow-none mb-3 float-right" data-toggle="modal" data-target="#inputMerk">
        <i class="fa fa-plus fa-fw"></i> Tambah Merk
      </button>
    </div>
  </div>
  <div class="bg-white shadow-sm rounded pt-5 pb-4 px-5">

    @if(session('alert'))
    <div class="alert alert-success shadow-sm">
      {{ session('alert') }}
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
    </div>
    @endif

    <table class="table table-striped" id="tolong">
      <thead>
        <tr>
          <th>#</th>
          <th>Kode Merk</th>
          <th>Nama Merk</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($merk as $m)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $m->KdMerk }}</td>
          <td>{{ $m->NmMerk }}</td>
          <td style="width:160px">
            <button class="btn btn-sm btn-warning shadow-none" data-toggle="modal" data-target="#editMerk"
              data-id="{{ $m->id }}" data-kodemerk="{{ $m->KdMerk }}" data-namamerk="{{ $m->NmMerk }}">
              <i class="fas fa-fw fa-edit"></i>
            </button>
            <button class="btn btn-sm btn-danger shadow-none" data-toggle="modal" data-target="#hapusMerk"
              data-id="{{ $m->id }}">
              <i class="fas fa-fw fa-trash"></i>
            </button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<!-- AWAL MODAL TAMBAH-->
<div class="modal fade" id="inputMerk">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-primary text-center">
        <h5 class="modal-title h5 w-100">TAMBAH DATA MERK</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="{{ route('admin.merk.simpan') }}" method="post">
          @csrf
          <div class="form-group">
            <label for="KdMerk">Kode Merk</label>
            <input type="text" class="form-control @error('KdMerk') is-invalid @enderror" id="KdMerk" name="KdMerk"
              autocomplete="off" value="{{ old('KdMerk') }}">
            <div class="invalid-feedback">
              Kode merk wajib diisi.
            </div>
          </div>
          <div class="form-group">
            <label for="NmMerk">Nama Merk</label>
            <input type="text" class="form-control @error('NmMerk') is-invalid @enderror" id="NmMerk" name="NmMerk"
              autocomplete="off" value="{{ old('NmMerk') }}">
            <div class="invalid-feedback">
              Kode merk wajib diisi.
            </div>
          </div>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="button" class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary shadow-none" id="simpan">Simpan Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR MODAL TAMBAH-->

<!-- AWAL MODAL EDIT-->
<div class="modal fade" id="editMerk">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-primary text-center">
        <h5 class="modal-title h5 w-100">UBAH DATA MERK</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <!-- AWAL FORM -->
        <form action="#" method="post" id="editMerkForm">
          @csrf
          @method('patch')
          <div class="form-group">
            <label for="KdMerk">Kode Merk</label>
            <input type="text" class="form-control" id="UbahKdMerk" disabled>
          </div>
          <div class="form-group">
            <label for="NmMerk">Nama Merk</label>
            <input type="text" class="form-control @error('UbahNmMerk') is-invalid @enderror" id="UbahNmMerk"
              name="UbahNmMerk" autocomplete="off">
            <div class="invalid-feedback">
              Nama merk wajib diisi.
            </div>
          </div>
          <!-- AKHIR FORM -->
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="button" class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary shadow-none" id="ubah">Ubah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR MODAL EDIT-->

<!-- AWAL HAPUS -->
<div class="modal fade center" id="hapusMerk">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-danger text-center">
        <h5 class="modal-title h5 w-100">HAPUS DATA MERK</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="#" method="post" id="hapusMerkForm">
          @csrf
          @method('delete')
          <center>
            <h5>Data ini akan dihapus. Apakah anda yakin?</h5>
          </center>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="submit" class="btn btn-danger shadow-none">Ya</button>
        <button type="button" class="btn btn-outline-danger shadow-none" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR HAPUS -->
@endsection

@section('script')
<script>
  $(document).ready(function(){
    // EDIT
    $('#editMerk').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var id = button.data('id') 
      var KdMerk = button.data('kodemerk') 
      var NmMerk = button.data('namamerk') 

      var modal = $(this)
      $('#editMerkForm').attr('action', '{{ route("admin.merk") }}/'+id)
      modal.find('.modal-body #UbahKdMerk').val(KdMerk)
      modal.find('.modal-body #UbahNmMerk').val(NmMerk)
    })
    // HAPUS
    $('#hapusMerk').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var id = button.data('id') 

      var modal = $(this)
      $('#hapusMerkForm').attr('action', '{{ route("admin.merk") }}/'+id)
    })
  })
</script>
@endsection