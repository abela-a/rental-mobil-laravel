@extends('layouts/dashboard')

@section('title', 'Mobil - Abidzar Car Rental')
@section('heading', 'Daftar Mobil')
@section('breadcrumb')
<div style="font-size:17px" class="text-white">
  <i class="fa fa-home fa-fw"></i>
  <span class="mx-3">|</span>
  <a href="{{ route('homepage') }}" class="text-white">Home</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <a href="{{ route('admin.dashboard') }}" class="text-white">Admin</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <span>Mobil</span>
</div>
@endsection

@section('content')
<!-- TOMBOL -->
<div class="container" id="main-menu">
  <div class="row mb-4">
    <div class="col-md">
      <button class="btn btn-primary shadow-none mb-3 float-right" type="button" data-toggle="modal"
        data-target="#inputMobil">
        <i class="fa fa-plus fa-fw"></i> Tambah Mobil
      </button>
    </div>
  </div>
  <div class="bg-white shadow-sm rounded pt-5 pb-4 px-5">
    @if(session('alert'))
    <div class="alert alert-success shadow-sm">
      {{ session('alert') }}
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
    </div>
    @endif
    <!-- TABLE -->
    <table class="table table-striped" id="tolong">
      <thead>
        <tr>
          <th>#</th>
          <th>No Polisi</th>
          <th>Foto</th>
          <th>Mobil</th>
          <th>Informasi</th>
          <th>Harga Sewa</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($mobil as $m)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ strtoupper($m->NoPlat) }}</td>
          <td>
            <img class="" width="100" src="{{ asset('img/fotomobil') }}/{{ $m->FotoMobil }}">
          </td>
          <td>
            {{ ucfirst($m->NmMerk) }}
            {{ ucfirst($m->NmType) }}
          </td>
          <td>
            <h6>
              @if ($m->StatusRental == 'Kosong')
              <span class="ml-2 shadow-none badge badge-success">{{ $m->StatusRental }}</span>
              @elseif($m->StatusRental == 'Dipesan')
              <span class="ml-2 shadow-none badge badge-warning">{{ $m->StatusRental }}</span>
              @else
              <span class="ml-2 shadow-none badge badge-danger">{{ $m->StatusRental }}</span>
              @endif
              <span class="badge indigo shadow-none">{{ $m->JenisMobil }}</span>
              <span class="badge badge-light shadow-none">{{ $m->Transmisi }}</span>
            </h6>
          </td>
          <td>Rp.<span class="uang">{{ $m->HargaSewa }}</span>,-</td>
          <td class="text-center" style="width:150px">
            <button class="btn btn-sm btn-warning shadow-none" data-toggle="modal"
              data-target="#editMobil-{{ $m->id }}">
              <i class="fas fa-fw fa-edit"></i>
            </button>
            <button class="btn btn-sm btn-danger shadow-none" data-toggle="modal" data-target="#hapusMobil"
              data-id="{{ $m->id }}">
              <i class="fas fa-fw fa-trash"></i>
            </button>
          </td>
        </tr>

        <!-- AWAL MODAL EDIT-->
        <div class="modal fade" id="editMobil-{{ $m->id }}">
          <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header text-primary text-center">
                <h5 class="modal-title h5 w-100">UBAH DATA MOBIL</h5>
              </div>
              <div class="modal-body px-5 grey lighten-5">
                <!-- AWAL FORM -->
                <form action="{{ route('admin.mobil') }}/{{ $m->id }}" method="post" enctype="multipart/form-data">
                  @csrf @method('patch')
                  <div class="form-group">
                    <label for="NoPlat">Nomor Polisi</label>
                    <input type="text" class="form-control" id="NoPlat" value="{{ strtoupper($m->NoPlat) }}" disabled>
                  </div>
                  <div class="form-group">
                    <label for="KdMerk">Merk</label>
                    <input type="hidden" name="KdMerk" value="{{ $m->KdMerk }}">
                    <input type="text" class="form-control" id="NmMerk" value="{{ $m->NmMerk }}" disabled>
                  </div>
                  <div class="form-group">
                    <label for="IdType">Tipe</label>
                    <input type="hidden" name="IdType" value="{{ $m->IdType }}">
                    <input type="text" class="form-control" id="NmType" value="{{ $m->NmType }}" disabled>
                  </div>
                  <div class="form-group">
                    <label for="JenisMobil">Jenis Mobil</label>
                    <select class="browser-default custom-select @error('JenisMobil') is-invalid @enderror "
                      name="JenisMobil" id="JenisMobil">
                      <option value="" selected disabled>Pilih Jenis Mobil</option>
                      <option @if ($m->JenisMobil=='MPV' ) selected @endif>MPV</option>
                      <option @if ($m->JenisMobil=='City' ) selected @endif>City</option>
                      <option @if ($m->JenisMobil=='Sedan' ) selected @endif>Sedan</option>
                      <option @if ($m->JenisMobil=='SUV' ) selected @endif>SUV</option>
                      <option @if ($m->JenisMobil=='Double Cabin' ) selected @endif>Double Cabin</option>
                      <option @if ($m->JenisMobil=='Other' ) selected @endif>Other</option>
                    </select>
                    <div class="invalid-feedback">
                      Jenis mobil wajib dipilih.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="Transmisi">Jenis Transmisi</label>
                    <select class="browser-default custom-select @error('Transmisi') is-invalid @enderror "
                      name="Transmisi" id="Transmisi">
                      <option value="" selected disabled>Pilih Transmisi Mobil</option>
                      <option @if ($m->Transmisi=='Matic' ) selected @endif>Matic</option>
                      <option @if ($m->Transmisi=='Manual' ) selected @endif>Manual</option>
                      <option @if ($m->Transmisi=='CVT' ) selected @endif>CVT</option>
                    </select>
                    <div class="invalid-feedback">
                      Transmisi mobil wajib dipilih.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="HargaSewa">Harga Sewa</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                      </div>
                      <input type="text" class="form-control uang @error('HargaSewa') is-invalid @enderror "
                        id="HargaSewa" name="HargaSewa" autocomplete="off" value="{{ $m->HargaSewa }}">
                      <div class="invalid-feedback">
                        Harga sewa mobil wajib diisi.
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FotoMobil">Foto</label>
                    <div class="row no-gutters">
                      <div class="col-md-3">
                        <img class="img-thumbnail" width="100" src="{{ asset('img/fotomobil') }}/{{ $m->FotoMobil }}">
                      </div>
                      <div class="col-md">
                        <div class="input-group mb-3">
                          <div class="custom-file @error('FotoMobil') is-invalid @enderror">
                            <input type="file" class="custom-file-input " id="FotoMobil" name="FotoMobil">
                            <label class="custom-file-label" for="inputFotoMobil"
                              aria-describedby="inputFotoMobil">Pilih
                              Berkas</label>
                          </div>
                        </div>
                        <div class="invalid-feedback">
                          Gambar tidak sesuai.
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- AKHIR FORM -->
              </div>
              <div class="modal-footer text-center justify-content-center">
                <button type="button" class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
                <button type="submit" class="btn btn-primary shadow-none" id="submit">Ubah Data</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- AKHIR MODAL EDIT-->

        @endforeach
      </tbody>
    </table>
    <!-- AKHIR TABLE -->
  </div>
</div>

<!-- AWAL MODAL-->
<div class="modal fade" id="inputMobil">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-primary text-center">
        <h5 class="modal-title h5 w-100" id="inputMobilLabel">TAMBAH DATA MOBIL</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="{{ route('admin.mobil.simpan') }}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label for="NoPlat">Nomor Polisi</label>
            <input type="text" class="form-control @error('NoPlat') is-invalid @enderror " id="NoPlat" name="NoPlat"
              autocomplete="off" value="{{ old('NoPlat') }}">
            <div class="invalid-feedback">
              Nomor polisi wajib diisi.
            </div>
          </div>
          <div class="form-group">
            <label for="KdMerk">Merk</label>
            <select class="browser-default custom-select @error('KdMerk') is-invalid @enderror " name="KdMerk"
              id="KdMerk">
              <option value="" selected disabled>Pilih Merk</option>
              @foreach ($merk as $mr)
              <option @if(old('KdMerk')==$mr->KdMerk) selected @endif value="{{ $mr->KdMerk }}">{{ $mr->NmMerk }}
              </option>
              @endforeach
            </select>
            <div class="invalid-feedback">
              Merk mobil wajib dipilih.
            </div>
          </div>
          <div class="form-group" id="type">
            <label for="IdType">Tipe</label>
            <select class="browser-default custom-select @error('IdType') is-invalid @enderror " name="IdType"
              id="IdType">
              <option value="" selected disabled>Pilih Merk Terlebih Dahulu</option>
            </select>
            <div class="invalid-feedback">
              Tipe mobil wajib dipilih.
            </div>
          </div>
          <div class="form-group">
            <label for="JenisMobil">Jenis Mobil</label>
            <select class="browser-default custom-select @error('JenisMobil') is-invalid @enderror " name="JenisMobil"
              id="JenisMobil">
              <option value="" selected disabled>Pilih Jenis Mobil</option>
              <option @if(old('JenisMobil')=='MPV' ) selected @endif>MPV</option>
              <option @if(old('JenisMobil')=='City' ) selected @endif>City</option>
              <option @if(old('JenisMobil')=='Sedan' ) selected @endif>Sedan</option>
              <option @if(old('JenisMobil')=='SUV' ) selected @endif>SUV</option>
              <option @if(old('JenisMobil')=='Double Cabin' ) selected @endif>Double Cabin</option>
              <option @if(old('JenisMobil')=='Other' ) selected @endif>Other</option>
            </select>
            <div class="invalid-feedback">
              Jenis mobil wajib dipilih.
            </div>
          </div>
          <div class="form-group">
            <label for="Transmisi">Jenis Transmisi</label>
            <select class="browser-default custom-select @error('Transmisi') is-invalid @enderror " name="Transmisi"
              id="Transmisi">
              <option value="" selected disabled>Pilih Jenis Transmisi</option>
              <option @if(old('Transmisi')=='Matic' ) selected @endif>Matic</option>
              <option @if(old('Transmisi')=='Manual' ) selected @endif>Manual</option>
              <option @if(old('Transmisi')=='CVT' ) selected @endif>CVT</option>
            </select>
            <div class="invalid-feedback">
              Transmisi mobil wajib dipilih.
            </div>
          </div>
          <div class="form-group">
            <label for="HargaSewa">Harga Sewa</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Rp.</span>
              </div>
              <input type="text" class="form-control uang @error('HargaSewa') is-invalid @enderror " id="HargaSewa"
                name="HargaSewa" autocomplete="off" value="{{ old('HargaSewa') }}">
              <div class="invalid-feedback">
                Harga sewa mobil wajib diisi.
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="FotoMobil">Foto</label>
            <div class="input-group mb-3">
              <div class="custom-file @error('FotoMobil') is-invalid @enderror">
                <input type="file" class="custom-file-input" id="FotoMobil" name="FotoMobil">
                <label class="custom-file-label" for="inputFotoMobil" aria-describedby="inputFotoMobil">Pilih
                  Berkas</label>
              </div>
            </div>
            <div class="invalid-feedback">
              Gambar tidak sesuai.
            </div>
          </div>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="button" class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary shadow-none" id="submit">Simpan Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR MODAL-->

<!-- AWAL HAPUS -->
<div class="modal fade center" id="hapusMobil">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-danger text-center">
        <h5 class="modal-title h5 w-100">HAPUS DATA MOBIL</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="#" method="post" id="hapusMobilForm">
          @csrf
          @method('delete')
          <center>
            <h5>Data ini akan dihapus. Apakah anda yakin?</h5>
          </center>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="submit" class="btn btn-danger shadow-none">Ya</button>
        <button type="button" class="btn btn-outline-danger shadow-none" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR HAPUS -->

@endsection

@section('script')
<script>
  $(document).ready(function(){
    // HAPUS
    $('#hapusMobil').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var id = button.data('id') 

      var modal = $(this)
      $('#hapusMobilForm').attr('action', '{{ route("admin.mobil") }}/'+id)
    })
    // LOAD TYPE
    $("#KdMerk").change(function() {
      var merk = $('#KdMerk').val();
      $('#type').load("{{ route('admin.mobil') }}/type/" + merk);
    })
  })
</script>
@endsection