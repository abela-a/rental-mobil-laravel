@extends('layouts/dashboard')

@section('title', 'Sopir - Abidzar Car Rental')
@section('heading', 'Daftar Sopir')
@section('breadcrumb')
<div style="font-size:17px" class="text-white">
  <i class="fa fa-home fa-fw"></i>
  <span class="mx-3">|</span>
  <a href="{{ route('homepage') }}" class="text-white">Home</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <a href="{{ route('admin.dashboard') }}" class="text-white">Admin</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <span>Sopir</span>
</div>
@endsection

@section('content')
<!-- TOMBOL -->
<div class="container" id="main-menu">
  <div class="row mb-4">
    <div class="col-md">
      <button class="btn btn-primary shadow-none mb-3 float-right" type="button" data-toggle="modal"
        data-target="#inputSopir">
        <i class="fa fa-plus fa-fw"></i> Tambah Sopir
      </button>
    </div>
  </div>
  <div class="bg-white shadow-sm rounded pt-5 pb-4 px-5">
    @if(session('alert'))
    <div class="alert alert-success shadow-sm">
      {{ session('alert') }}
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
    </div>
    @endif
    <!-- TABLE -->
    <table class="table table-striped" id="tolong">
      <thead>
        <tr>
          <th>#</th>
          <th>NIK</th>
          <th>ID</th>
          <th>Nama</th>
          <th>Tarif/Hari</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($sopir as $s)

        @php
        if($s->StatusSopir == 'Sibuk'){
        $status = 'badge-danger';
        } elseif($s->StatusSopir == 'Dipesan'){
        $status = 'badge-warning';
        } else {
        $status = 'badge-success';
        }
        @endphp

        <tr class="@if($s->IdSopir == 'SPR000') d-none @endif">
          <td>{{ $loop->index }}</td>
          <td>{{ $s->NIK }}</td>
          <td>{{ strtoupper($s->IdSopir) }}</td>
          <td>
            {{ $s->NmSopir }}
            <span class="ml-2 shadow-none badge {{$status}}">{{ $s->StatusSopir }}</span>
          </td>
          <td>Rp.<span class="uang">{{ $s->TarifPerhari }}</span>,-</td>
          <td class="text-center" style="width:220px">
            <button class="btn btn-sm btn-warning shadow-none" data-toggle="modal"
              data-target="#editSopir-{{ $s->id }}">
              <i class="fas fa-fw fa-edit"></i>
            </button>
            <button class="btn btn-sm btn-danger shadow-none" data-toggle="modal" data-target="#hapusSopir"
              data-id="{{ $s->id }}">
              <i class="fas fa-fw fa-trash"></i>
            </button>
            <button class="btn btn-sm btn-info shadow-none" data-toggle="modal" data-target="#detailSopir-{{ $s->id }}">
              <i class="fas fa-fw fa-user"></i>
            </button>
          </td>
        </tr>

        <!-- AWAL MODAL EDIT-->
        <div class="modal fade" id="editSopir-{{ $s->id }}">
          <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header text-primary text-center">
                <h5 class="modal-title h5 w-100">UBAH DATA SOPIR</h5>
              </div>
              <div class="modal-body px-5 grey lighten-5">
                <form action="{{ route('admin.sopir') }}/{{ $s->id }}" method="post">
                  @csrf @method('patch')
                  <div class="form-group">
                    <label for="NIK">Nomor Induk Kependudukan</label>
                    <input type="text" class="form-control" id="NIK" value="{{ $s->NIK }}" disabled>
                  </div>
                  <div class="form-group">
                    <label for="NoSim">Nomor SIM</label>
                    <input type="text" class="form-control" id="NoSim" value="{{ $s->NoSim }}" disabled>
                  </div>
                  <div class="form-group">
                    <label for="NmSopir">Nama Lengkap</label>
                    <input type="text" class="form-control @error('NmSopir') is-invalid @enderror" name="NmSopir"
                      id="NmSopir" autocomplete="off" value="{{ $s->NmSopir }}">
                    <div class="invalid-feedback">
                      Nama lengkap sopir wajib diisi.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="JenisKelamin">Jenis Kelamin</label>
                    <select name="JenisKelamin" id="JenisKelamin"
                      class="form-control browser-default @error('JenisKelamin') is-invalid @enderror">
                      <option disabled selected>Pilih Jenis Kelamin</option>
                      <option @if($s->JenisKelamin=="L" ) selected @endif value="L">Laki-laki</option>
                      <option @if($s->JenisKelamin=="P" ) selected @endif value="P">Perempuan</option>
                    </select>
                    <div class="invalid-feedback">
                      Jenis kelamin wajib dipilih.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="Alamat">Alamat</label>
                    <textarea name="Alamat" id="Alamat" class="form-control @error('Alamat') is-invalid @enderror"
                      autocomplete="off">{{ $s->Alamat }}</textarea>
                    <div class="invalid-feedback">
                      Alamat sopir wajib diisi.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="NoTelp">Nomor Telepon</label>
                    <input type="text" class="form-control telp @error('NoTelp') is-invalid @enderror" name="NoTelp"
                      id="NoTelp" autocomplete="off" value="{{ $s->NoTelp }}">
                    <div class="invalid-feedback">
                      Nomor HP/telepon sopir wajib diisi.
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="TarifPerhari">Tarif Perhari</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                      </div>
                      <input type="text" class="form-control uang @error('TarifPerhari') is-invalid @enderror"
                        name="TarifPerhari" id="TarifPerhari" autocomplete="off" value="{{ $s->TarifPerhari }}">
                      <div class="invalid-feedback">
                        Tarif/Hari sopir wajib diisi.
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer text-center justify-content-center">
                <button type="button" class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
                <button type="submit" class="btn btn-primary shadow-none" id="submit">Ubah Data</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- AKHIR MODAL EDIT-->

        <!-- AWAL MODAL DETAIL -->
        <div class="modal fade right" id="detailSopir-{{ $s->id }}">
          <div class="modal-dialog modal-full-height modal-right">
            <div class="modal-content">
              <div class="modal-header text-center text-primary">
                <h4 class="modal-title w-100 h5">DETAIL SOPIR</h4>
              </div>
              <div class="modal-body px-5 grey lighten-5">
                <ul class="list-group list-group-flush">
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">ID Sopir</div>
                    <div class="col" style="font-weight:500">{{ $s->IdSopir }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Nomor Induk Kependudukan</div>
                    <div class="col" style="font-weight:500">{{ $s->NIK }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Nama Sopir</div>
                    <div class="col" style="font-weight:500">{{ $s->NmSopir }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">NO SIM</div>
                    <div class="col" style="font-weight:500">{{ $s->NoSim }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Jenis Kelamin</div>
                    <div class="col" style="font-weight:500">
                      @if ($s->JenisKelamin == 'L')
                      Laki-laki
                      @else
                      Perempuan
                      @endif
                    </div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">No Telepon</div>
                    <div class="col telp" style="font-weight:500">{{ $s->NoTelp }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Alamat</div>
                    <div class="col" style="font-weight:500">{{ $s->Alamat }}</div>
                  </div>
                  <div class="row list-group-item grey lighten-5">
                    <div class="col">Tarif / Hari</div>
                    <div class="col" style="font-weight:500;color:red">
                      Rp.<span class="uang">{{ $s->TarifPerhari }}</span>,-
                    </div>
                  </div>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- AKHIR MODAL DETAIL -->

        @endforeach
      </tbody>
    </table>
    <!-- AKHIR TABLE -->
  </div>
</div>

<!-- AWAL MODAL-->
<div class="modal fade" id="inputSopir">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-primary text-center">
        <h5 class="modal-title h5 w-100" id="inputSopirLabel">TAMBAH DATA SOPIR</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="{{ route('admin.sopir.simpan') }}" method="post">
          @csrf
          <div class="form-group">
            <label for="NIK">Nomor Induk Kependudukan</label>
            <input type="text" class="form-control @error('NIK') is-invalid @enderror" name="NIK" id="NIK"
              autocomplete="off" value="{{ old('NIK') }}">
            <div class="invalid-feedback">
              NIK wajib diisi dengan panjang 13 karakter.
            </div>
          </div>
          <div class="form-group">
            <label for="NoSim">Nomor SIM</label>
            <input type="text" class="form-control @error('NoSim') is-invalid @enderror" name="NoSim" id="NoSim"
              autocomplete="off" value="{{ old('NoSim') }}">
            <div class="invalid-feedback">
              Nomor SIM sopir wajib diisi.
            </div>
          </div>
          <div class="form-group">
            <label for="NmSopir">Nama Lengkap</label>
            <input type="text" class="form-control @error('NmSopir') is-invalid @enderror" name="NmSopir" id="NmSopir"
              autocomplete="off" value="{{ old('NmSopir') }}">
            <div class="invalid-feedback">
              Nama lengkap sopir wajib diisi.
            </div>
          </div>
          <div class="form-group">
            <label for="JenisKelamin">Jenis Kelamin</label>
            <select name="JenisKelamin" id="JenisKelamin"
              class="form-control browser-default @error('JenisKelamin') is-invalid @enderror">
              <option disabled selected>Pilih Jenis Kelamin</option>
              <option @if(old('JenisKelamin')=="L" ) selected @endif value="L">Laki-laki</option>
              <option @if(old('JenisKelamin')=="P" ) selected @endif value="P">Perempuan</option>
            </select>
            <div class="invalid-feedback">
              Jenis kelamin wajib dipilih.
            </div>
          </div>
          <div class="form-group">
            <label for="Alamat">Alamat</label>
            <textarea name="Alamat" id="Alamat" class="form-control @error('Alamat') is-invalid @enderror"
              autocomplete="off">{{ old('Alamat') }}</textarea>
            <div class="invalid-feedback">
              Alamat sopir wajib diisi.
            </div>
          </div>
          <div class="form-group">
            <label for="NoTelp">Nomor Telepon</label>
            <input type="text" class="form-control telp @error('NoTelp') is-invalid @enderror" name="NoTelp" id="NoTelp"
              autocomplete="off" value="{{ old('NoTelp') }}">
            <div class="invalid-feedback">
              Nomor HP/telepon sopir wajib diisi.
            </div>
          </div>
          <div class="form-group">
            <label for="TarifPerhari">Tarif Perhari</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Rp</span>
              </div>
              <input type="text" class="form-control uang @error('TarifPerhari') is-invalid @enderror"
                name="TarifPerhari" id="TarifPerhari" autocomplete="off" value="{{ old('TarifPerhari') }}">
              <div class="invalid-feedback">
                Tarif/Hari sopir wajib diisi.
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="button" class="btn btn-outline-primary shadow-none" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary shadow-none" id="submit">Simpan Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR MODAL-->

<!-- AWAL HAPUS -->
<div class="modal fade center" id="hapusSopir">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-danger text-center">
        <h5 class="modal-title h5 w-100">HAPUS DATA SOPIR</h5>
      </div>
      <div class="modal-body px-5 grey lighten-5">
        <form action="#" method="post" id="hapusSopirForm">
          @csrf
          @method('delete')
          <center>
            <h5>Data ini akan dihapus. Apakah anda yakin?</h5>
          </center>
      </div>
      <div class="modal-footer text-center justify-content-center">
        <button type="submit" class="btn btn-danger shadow-none">Ya</button>
        <button type="button" class="btn btn-outline-danger shadow-none" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- AKHIR HAPUS -->

@endsection

@section('script')
<script>
  $(document).ready(function(){
    // HAPUS
    $('#hapusSopir').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var id = button.data('id') 

      var modal = $(this)
      $('#hapusSopirForm').attr('action', '{{ route("admin.sopir") }}/'+id)
    })
  })
</script>
@endsection