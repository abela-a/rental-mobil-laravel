@extends('layouts/dashboard')

@section('title', 'Role - Abidzar Car Rental')
@section('heading', 'Daftar Akun')
@section('breadcrumb')
<div style="font-size:17px" class="text-white">
  <i class="fa fa-home fa-fw"></i>
  <span class="mx-3">|</span>
  <a href="{{ route('homepage') }}" class="text-white">Home</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <a href="{{ route('admin.dashboard') }}" class="text-white">Admin</a>
  <i class="fa fa-angle-right fa-fw mx-2"></i>
  <span>Role</span>
</div>
@endsection

@section('content')
<div class="container" id="main-menu">
  <div class="row mb-4">
    <div class="col-md clear-fix">
      <button class="btn mb-3 shadow-none float-right" type="button" data-toggle="modal" data-target="#inputUser">
      </button>
    </div>
  </div>
  <div class="bg-white shadow-sm rounded pt-5 pb-4 px-5">
    @if(session('alert'))
    <div class="alert alert-success shadow-sm">
      {{ session('alert') }}
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
    </div>
    @endif
    <table class="table table-hover" id="tolong">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col" class="text-center">NIK</th>
          <th scope="col" class="text-center">Nama</th>
          <th scope="col" class="text-center">Role</th>
          <th scope="col" class="text-center">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($role as $r)
        <tr>
          <form action="{{ route('admin.role') }}/{{ $r->id }}" method="post">

            @csrf @method('patch')

            <td>{{ $loop->iteration }}</td>
            <td>{{ ucfirst($r->NIK) }}</td>
            <td>{{ ucfirst($r->nama) }}</td>
            <td>
              <!-- Role -->
              <select class="browser-default custom-select" name="role" id="role">
                <option value="1" @if($r->RoleId === 1) selected @endif >Admin</option>
                <option value="2" @if($r->RoleId === 2) selected @endif >Karyawan</option>
                <option value="3" @if($r->RoleId === 3) selected @endif >Pelanggan</option>
              </select>
            </td>
            <td class="text-center" style="width:100px">
              <button class="btn btn-sm btn-success text-white shadow-none" type="submit" title="Update Role">
                <i class=" fa fa-check" aria-hidden="true"></i>
              </button>
            </td>
          </form>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('script')
@endsection