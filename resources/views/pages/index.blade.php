@extends('layouts.home')

@section('title', 'Abidzar Car Rental')

@section('content')
<!-- AWAL LANDING -->
<div class="mt-4">
    <!-- JUMBOTRON -->
    <div class="view jarallax jumbotron card card-image" style="height:100vh">
        <!-- JARALLAX -->
        <img src="{{ asset('img/assets/bg.jpg') }}" alt="" class="jarallax-img">
        <!-- MASK WARNA -->
        <div class="mask flex-center rgba-indigo-strong">
            <div class="text-white container px-4">
                <div class="row">
                    <div class="col-md-5 col-sm-12">
                        <h3 style="font-size:50px;" class="font-weight-bold">
                            Abidzar Car Rental
                        </h3>
                        <span id="motto">
                            <i> Your Car Rental Solution</i>
                        </span>
                        <div id="home" class="pr-5 font-weight-regular">
                            <p class="mt-4">
                                Kami adalah penyedia layanan sewa mobil di Makassar yang sudah berpengalaman dalam
                                menyediakan mobil berkualitas dengan harga yang murah sejak tahun 2019.
                            </p>
                            <br>
                            <p>
                                Kami siap melayani kebutuhan Anda selama di Makassar untuk tujuan wisata, mudik, urusan
                                pekerjaan, pindahan, wisuda, keluar kota, dan berbagai kebutuhan yang Anda perlukan.
                            </p>
                            <br>
                            <p>
                                Kami selalu berkomitmen untuk memberikan pelayanan <b>terbaik</b> dengan mobil yang
                                <b>terawat</b> dan sopir yang <b>kompeten</b>.
                            </p>
                        </div>
                        <div class="clear-fix mt-4">
                            {{-- UDAH LOGIN --}}
                            @auth
                            @if(Auth::user()->RoleId === 1 || Auth::user()->RoleId === 2)
                            <a class="btn indigo accent-2" href="{{ $dashboard ?? '' }}">Dashboard</a>
                            @else
                            <a class="btn indigo accent-2" href="/pelanggan/daftarmobil">Pesan Mobil</a>
                            @endif
                            @endauth
                            {{-- BELUM LOGIN --}}
                            @guest
                            <a href="{{ route('register') }}" class="btn btn-warning">Daftar</a>
                            <a href="{{ route('login') }}" class="btn btn-outline-white">login</a>
                            @endguest
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <img src="{{ asset('img/assets/landing.png') }}" id="landing-img">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- AKHIR LANDING -->

<div class="container position-relative">

    <!-- AWAL MEMILIH KAMI -->
    <section id="alasan" class="px-5 py-5 bg-white rounded shadow-sm">

        <h4 class="text-center h3">Mengapa Memilih Kami ?</h4>
        <div class="row mt-5">
            <!-- HARGA -->
            <div class="col-md-4 col-sm-12">
                <div class="text-center">
                    <i class="fas fa-money-bill-wave-alt fa-4x text-warning d-block mb-3"></i>
                    <span class="h5">Harga Terjangkau</span>
                    <p class="px-4 mt-3">
                        Harga sewa mobil terjangkau di Makassar hanya ada di perusahaan kami, jangan ragu untuk
                        menentukan rencana sewa mobil di Makassar, satukan pilihan Anda juga termasuk layanan Sopir kami
                        berpengalaman.
                    </p>
                </div>
            </div>

            <!-- LAYANAN -->
            <div class="col-md-4 col-sm-12">
                <div class="text-center">
                    <i class="fas fa-headset fa-4x text-warning d-block mb-3"></i>
                    <span class="h5">Pelayanan Prima</span>
                    <p class="px-4 mt-3">
                        Costumer sevice kami siap melayani Anda kapan pun. Dan siap dengan media apapun, mulai dari via
                        SMS, Whatsapp, E-mail, maupun telepon secara langsung.
                    </p>
                </div>
            </div>

            <!-- KUALITAS -->
            <div class="col-md-4 col-sm-12">
                <div class="text-center">
                    <i class="fas fa-user-tie fa-4x text-warning d-block mb-3"></i>
                    <span class="h5">Kualitas Terbaik</span>
                    <p class="px-4 mt-3">
                        Perusahaan kami didukung dengan tim yang profesional dan dikelola oleh SDM pilihan serta unit
                        mobil yang variatif, mulai dari city car, sampai mobil rombongan dan mobil mewah untuk
                        pengantin.
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- AKHIR MEMILIH KAMI -->
</div>
@endsection