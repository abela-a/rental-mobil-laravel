@extends('layouts.auth')

@section('title', 'Register - Abidzar Car Rental')

@section('content')
<div id="login-register-img" style="background-image: url({{ asset('img/assets/bg-login-register.jpg') }});">
  <div class="rgba-white-strong h-100">
    <div class="container">
      <div class="row justify-content-md-center no-gutters">
        <div class="col-md-5 mt-5 shadow-lg mb-0">

          <div class="card-image h-100"
            style="background-image: url({{ asset('img/assets/card-login-register.jpg') }});">
            <div class="text-white text-center flex-center rgba-indigo-strong py-5 px-4">
              <div>
                <a class="text-white py-0" href="{{ route('homepage') }}">
                  <img src="{{ asset('img/assets/logo.png') }}" width="40" height="40" class="d-inline-block align-top"
                    alt="">
                  <span style="font-size:25px" class="font-weight-bold">Abidzar</span>
                  <span style="font-size:18px" class="font-weight-thin">Car Rental</span>
                </a>
                <h1 class="mt-5">
                  MENGAPA
                  <br> MEMILIH
                  <br> KAMI ?
                </h1>
                <p class="mt-4 px-3">Kami adalah penyedia layanan sewa mobil di Makassar yang sudah berpengalaman dalam
                  menyediakan mobil berkualitas dengan harga yang murah sejak tahun 2019.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5 mt-5 shadow-lg h-100">
          <!-- BODY -->
          <form action="{{ route('register') }}" method="post">

            @csrf

            <div class="grey lighten-5">

              <div class="py-4 px-5">
                <div class="py-3 px-3 text-center">
                  <span class="text-center h3 text-primary font-weight-medium">REGISTRATION PAGE</span>
                </div>
              </div>

              <div style="overflow-y: scroll; height:300px;" class="px-5">

                <div class="form-group">
                  <label for="nama">Nama Lengkap</label>
                  <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="nama"
                    autocomplete="off" value="{{ old('nama') }}" autofocus>
                  <div class="invalid-feedback">
                    Nama lengkap wajib diisi.
                  </div>
                </div>

                <div class="form-group">
                  <label for="NIK">Nomor Induk Kependudukan</label>
                  <input class="form-control @error('NIK') is-invalid @enderror" type="number" name="NIK" id="NIK"
                    autocomplete="off" value="{{ old('NIK') }}">
                  <div class="invalid-feedback">
                    NIK wajib di isi dengan panjang 13 angka.
                  </div>
                </div>

                <div class="form-group">
                  <label for="email">Email</label>
                  <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" id="email"
                    autocomplete="off" value="{{ old('email') }}">
                  <div class="invalid-feedback">
                    Email harus valid dan wajib diisi.
                  </div>
                </div>

                <div class="form-group">
                  <label for="password">Password</label>
                  <input class="form-control @error('password') is-invalid @enderror" type="password" name="password"
                    id="password">
                  <div class="invalid-feedback">
                    Password minimal 8 karakter.
                  </div>
                </div>

                <div class="form-group">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                    autocomplete="new-password" placeholder="Konfirmasi Password">
                </div>

                <div class="form-group">
                  <label for="JenisKelamin">Jenis Kelamin</label>
                  <select class="browser-default custom-select @error('JenisKelamin') is-invalid @enderror"
                    name="JenisKelamin" id="JenisKelamin">
                    <option value="" selected>Pilih jenis Kelamin</option>
                    <option @if(old('JenisKelamin')=='L' ) selected @endif value="L">Laki-laki</option>
                    <option @if(old('JenisKelamin')=='P' ) selected @endif value="P">Perempuan</option>
                  </select>
                  <div class="invalid-feedback">
                    Jenis kelamin wajib dipilih.
                  </div>
                </div>

                <div class="form-group">
                  <label for="Alamat">Alamat</label>
                  <textarea class="form-control @error('Alamat') is-invalid @enderror" type="number" name="Alamat"
                    id="Alamat" autocomplete="off">{{ old('Alamat') }}</textarea>
                  <div class="invalid-feedback">
                    Alamat wajib diisi.
                  </div>
                </div>

                <div class="form-group">
                  <label for="NoTelp">No Telepon</label>
                  <input class="form-control @error('NoTelp') is-invalid @enderror telp" type="text" name="NoTelp"
                    id="NoTelp" autocomplete="off" value="{{ old('NoTelp') }}">
                  <div class="invalid-feedback">
                    Nomor HP/telepon wajib diisi.
                  </div>
                </div>

              </div>

              <div class="px-5 pb-4">
                <button class="btn btn-primary btn-block my-3" type="submit">Daftar</button>

          </form>
          <small>
            Sudah punya akun?
            <a href="{{ route('login') }}" class="text-primary" style="font-weight:500">
              Login!
            </a>
          </small>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection